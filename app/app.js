import Resolver from 'ember/resolver';


// Ember App Kit doesn't seem to automatically import Views
import EditLabelView from 'appkit/views/edit_label';

Ember.MODEL_FACTORY_INJECTIONS = true;

var App = Ember.Application.extend({
  LOG_ACTIVE_GENERATION: true,
  LOG_MODULE_RESOLVER: true,
  LOG_TRANSITIONS: true,
  LOG_TRANSITIONS_INTERNAL: true,
  LOG_VIEW_LOOKUPS: true,
  modulePrefix: 'appkit', // TODO: loaded via config
  Resolver: Resolver['default']
});



// App.Store = DS.Store.extend({
//     adapter: DS.FixtureAdapter
// });

export default App;
