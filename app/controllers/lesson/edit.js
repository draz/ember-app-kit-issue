var LessonController = Em.ObjectController.extend({
  // needs: [],
  isEditing: false,

  actions: {
    addScean: function () {
      var lesson = this.get('model'),
          sceans = lesson.get('sceans');

      var scean = this.store.createRecord('scean', {
         name:"Scean "+ (sceans.get('length')+1),
         lesson: lesson
        });
      scean.save().then(function () {
        lesson.get('sceans').pushObject(scean);
        lesson.save();
      });
      // console.log(this.get('sceans'));
      // this.get('sceans').then(function (sceanss) {
      // sceans.addObject(scean);
      //   lesson.save();
      // });
     // lesson.save();
      // this.transitionToRoute('scean', lesson, scean);
    },
    removeScean: function (scean) {
      this.get('model.sceans').removeObject(scean);
      scean.destroyRecord();
      return false;

    }
  }
});

export default LessonController;