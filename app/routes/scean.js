export default Em.Route.extend({
  // activate: function() {},
  // deactivate: function() {},
  // setupController: function(controller, model) {},
  // renderTemplate: function() {},
  // beforeModel: function() {},
  // afterModel: function() {},
  setupController: function(controller, model) {
    controller.set('model', model);
    controller.set('canEdit', false);
  },
  model: function(params) {
    // console.log(params); this is the working one
      return this.store.find('scean', params.scean_id);
  }
});
