var SceanEditRoute = Em.Route.extend({
  // activate: function() {},
  // deactivate: function() {},
  renderTemplate: function() {
    this.render('scean');
  },
  // beforeModel: function() {},
  // afterModel: function() {},
  setupController: function(controller, model) {
    controller = this.controllerFor('scean');
    controller.set('model', model);
    controller.set('canEdit', true);
  },
  model: function() {
      return this.modelFor('scean');;
  }
});


export default SceanEditRoute;