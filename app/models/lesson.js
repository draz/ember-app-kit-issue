import App from "appkit/app";

var Lesson = DS.Model.extend({
  title: DS.attr('string'),
  color: DS.attr('string'),
  date: DS.attr('date'),
  sceans: DS.hasMany('scean', {async:true})
});


Lesson.FIXTURES = [
 {
    id:1,
    title: "Lesson 1",
    color:"#a3f",
    startScean: 1,
    sceans : [1,2,4,5]
 },
 {
    id:2,
    title: "Lesson 2",
    color:"#3af",
    sceans: [3]
 }
];

App.Lesson = Lesson;

export default Lesson;