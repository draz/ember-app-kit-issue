import App from "appkit/app";

var SceanModel = DS.Model.extend({
  name: DS.attr('string'),
  lesson: DS.belongsTo('lesson')
  // reactions: DS.hasMany('reaction')
});



SceanModel.FIXTURES = [
    {
        id:1,
        name: "Scean 1",
        lesson: 1
    },
    {
        id:2,
        name: "Scean 2",
        lesson: 1
    },
    {
        id:4,
        name: "Scean 3",
        lesson: 1
    },
    {
        id:5,
        name: "The End",
        lesson: 1
    },
     {
        id:3,
        name: "Scean 1",
        lesson: 2
    }
];

App.Scean = SceanModel;

export default SceanModel;