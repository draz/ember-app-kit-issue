export default DS.JSONSerializer.extend({
  // fixes extractArray bug in default JSONSerializer
  extractArray: function(store, type, payload) {
    return payload.map(function(json) {
      return this.extractSingle(store, type, json);
    }, this);
  },
  
  // fixes manyToOne in default JSONSerializer
  serializeHasMany: function(record, json, relationship) {
    var key = relationship.key;

    var relationshipType = DS.RelationshipChange.determineRelationshipType(record.constructor, relationship);
    var relationshipTypes = ['manyToNone', 'manyToMany', 'manyToOne'];

    if (relationshipTypes.contains(relationshipType)) {
      json[key] = Em.get(record, key).mapBy('id');
    }
  }
});