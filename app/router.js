var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
  // this.route('component-test');
  // this.route('helper-test');

  //this.resource('/' function () {
    this.resource('lesson', {path:"lesson/:lesson_id"}, function () {
      this.route('preview');
      this.route('edit');
    });
    
  //})
});

export default Router;
